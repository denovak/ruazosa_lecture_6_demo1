package hr.fer.ruazosa.lecture5

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var counterState: Int = 0
    var isOperationStarted: Boolean = false
    var countToNumber: Int = 1000

    var operation = { v:View ->
        isOperationStarted = true
        Thread {
            for (i in counterState..countToNumber) {
                Thread.sleep(1000)
                runOnUiThread {
                    operationStatusTextView.text = i.toString()
                }
                counterState = i

            }
            isOperationStarted = false
        }.start()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startLongRunningOperationButton.setOnClickListener(View.OnClickListener(operation))


        editSettingButton.setOnClickListener {
            val intent = Intent(this, EditSettings::class.java)
            startActivityForResult(intent, EDIT_SETTINGS)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == EDIT_SETTINGS) {
            if (resultCode == SETTINGS_CHANGE_MADE) {
                val sharedPref = getSharedPreferences("shared", Context.MODE_PRIVATE)
                countToNumber = sharedPref.getInt(COUNTER_KEY_SETTINGS, 15)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(COUNTER_KEY, counterState)
        outState.putBoolean(OPERATION_STATUS_KEY, isOperationStarted)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        counterState = savedInstanceState.getInt(COUNTER_KEY)
        isOperationStarted = savedInstanceState.getBoolean(OPERATION_STATUS_KEY)

        if (isOperationStarted) {
            operation(startLongRunningOperationButton)
        }
    }


    companion object {
        const val COUNTER_KEY = "counter"
        const val OPERATION_STATUS_KEY = "operationStatus"
        const val EDIT_SETTINGS = 1
        const val COUNTER_KEY_SETTINGS = "counterSetting"
        const val SETTINGS_CHANGE_MADE = 2
        const val SETTINGS_CHANGE_CANCELLED = 3
    }

}
