package hr.fer.ruazosa.lecture5

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit_settings.*

class EditSettings : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_settings)


        saveSettingButton.setOnClickListener {
            val sharedPref = getSharedPreferences("shared", Context.MODE_PRIVATE)

            val editor = sharedPref.edit()
            editor.putInt(MainActivity.COUNTER_KEY_SETTINGS,
                timeToCountEditText.text.toString().toInt())
            editor.commit()

            val resultIntent = Intent()
            setResult(MainActivity.SETTINGS_CHANGE_MADE)
            finish()
        }


    }
}
